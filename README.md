## Sustituyendo letras en string

Para este ejercicio recuerda el uso de los métodos de string en ruby.

Define el método `world_cup` que sustituye las letras `t` y `o` de un string por las letras `s` y `a` respectivamente, siempre y cuando incluye la palabra `Rutio`. Las pruebas deben ser true.

```ruby
#world_cup method


#driver code
p world_cup("Clave: Rutio") == "Clave: Rusia"
p world_cup("Clave: Rutia") == "Wrong Password"
```
